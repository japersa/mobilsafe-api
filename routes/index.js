const express = require("express");
const router = express.Router();
const multer = require("multer");
const inMemoryStorage = multer.memoryStorage();
const singleFileUpload = multer({ storage: inMemoryStorage });
const azureStorage = require("azure-storage");
const getStream = require("into-stream");
const fs = require("fs");

const azureStorageConfig = {
  accountName: "ecocargapp",
  accountKey:
    "YWcufCvJgtyKmQHMwyVnp2LE7Y25Ye4MdTcFJG/Qacp1VRyQU3XmmQvcRB3HfgFu+C+M/bvMdPEJRZLMWZipWQ==",
  blobURL: "https://ecocargapp.blob.core.windows.net/",
  containerName: "ecocargapp",
};

uploadFile = async (directoryPath, fileName, file) => {
  return new Promise((resolve, reject) => {
    const buffer = new Buffer(file, "base64");

    const blobService = azureStorage.createBlobService(
      azureStorageConfig.accountName,
      azureStorageConfig.accountKey
    );
    blobService.createBlockBlobFromText(
      azureStorageConfig.containerName,
      `${directoryPath}/${fileName}`,
      buffer,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            filename: fileName,
            path: `${azureStorageConfig.containerName}/${directoryPath}/${fileName}`,
            url: `${azureStorageConfig.blobURL}${azureStorageConfig.containerName}/${directoryPath}/${fileName}`,
          });
        }
      }
    );
  });
};

uploadFileToBlob = async (directoryPath, file) => {
  return new Promise((resolve, reject) => {
    const blobName = file.originalname;
    const stream = getStream(file.buffer);
    const streamLength = file.buffer.length;

    const blobService = azureStorage.createBlobService(
      azureStorageConfig.accountName,
      azureStorageConfig.accountKey
    );
    blobService.createBlockBlobFromStream(
      azureStorageConfig.containerName,
      `${directoryPath}/${blobName}`,
      stream,
      streamLength,
      (err) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            filename: blobName,
            originalname: file.originalname,
            size: streamLength,
            path: `${azureStorageConfig.containerName}/${directoryPath}/${blobName}`,
            url: `${azureStorageConfig.blobURL}${azureStorageConfig.containerName}/${directoryPath}/${blobName}`,
          });
        }
      }
    );
  });
};

const imageUploadToBlob = async (req, res, next) => {
  try {
    const image = await uploadFileToBlob("images", req.file); // images is a directory in the Azure container
    return res.json(image);
  } catch (error) {
    next(error);
  }
};

router.post("/upload", singleFileUpload.single("file"), imageUploadToBlob);

module.exports = router;
